package it.mc.monitorme.controller;

import it.mc.monitorme.MonitorMeCommand;
import it.mc.monitorme.view.MonitorView;
import it.mc.monitorme.view.MonitorViewObserver;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;

/**
 * Reactive gui controller.
 *
 */
public class Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

    private final MonitorView view;
    private Agent agent;
    private long alertThreshold;
    private boolean sendEmailAlert;
    private String gmailUsername;
    private String gmailPassword;
    private String emailRecipient;

    /**
     * Construct a new controller.
     *
     * @param view
     *            the view
     * 
     * @param props
     *            the properties for configuring this controller
     */
    public Controller(final MonitorView view, final Properties props) {
        this.view = view;
        this.view.addMonitorViewObserver(this.new SimpleMonitorViewObserver());
        this.initProperties(props);
        view.initView();
    }

    private void initProperties(final Properties props) {
        this.alertThreshold = Long.parseLong(props.getProperty("alertThreshold"));
        final Optional<String> optSendEmail = Optional.ofNullable(props.getProperty("emailAlert"));
        this.sendEmailAlert = Boolean.parseBoolean(optSendEmail.orElse("false"));
        if (this.sendEmailAlert) {
            this.gmailUsername = props.getProperty("gmailUsername");
            this.gmailPassword = props.getProperty("gmailPassword");
            this.emailRecipient = props.getProperty("emailReceiver");
        }
    }

    private class Agent implements Runnable {

        // stop viene modificato "da fuori": sia volatile!!
        private volatile boolean stop;
        private MemoryMXBean memoryMXBean;
        private volatile boolean alertSent;
        private final String jvmPid;

        public Agent(final String pid) {
            this.jvmPid = pid;
            VirtualMachine vm;
            try {
                vm = VirtualMachine.attach(pid);
                final String agent = vm.getSystemProperties().getProperty("java.home") + File.separator + "lib"
                        + File.separator + "management-agent.jar";
                vm.loadAgent(agent);
                final String connectorAddr = vm.getAgentProperties().getProperty(
                        "com.sun.management.jmxremote.localConnectorAddress");
                final JMXServiceURL serviceURL = new JMXServiceURL(connectorAddr);
                final JMXConnector connector = JMXConnectorFactory.connect(serviceURL);
                final MBeanServerConnection mbsc = connector.getMBeanServerConnection();
                final ObjectName memoryMBean = new ObjectName(ManagementFactory.MEMORY_MXBEAN_NAME);
                final Set<ObjectName> mbeans = mbsc.queryNames(memoryMBean, null);
                for (final ObjectName name : mbeans) {
                    this.memoryMXBean = ManagementFactory.newPlatformMXBeanProxy(mbsc, name.toString(),
                            MemoryMXBean.class);
                }
            } catch (AttachNotSupportedException | IOException | AgentLoadException | AgentInitializationException
                    | MalformedObjectNameException e) {
                LOGGER.debug("Impossible to start monitor agent: {}", e);
            }
        }

        /**
         * Behavior of thread.
         */
        public void run() {
            while (!this.stop) {
                try {
                    final long value = this.memoryMXBean.getHeapMemoryUsage().getUsed();
                    if (!alertSent && value > Controller.this.alertThreshold) {
                        final String msg = "Heap memory usage on JVM " + this.jvmPid
                                + " has exceeded the threshold value. Current: " + value / 1024 + " KB. Threshold: "
                                + Controller.this.alertThreshold / 1024 + " KB";
                        Controller.this.view.displayAlert(msg);
                        Controller.this.sendAlertMail("Threshold exceeded", msg);
                        this.alertSent = true;
                    }
                    if (value < Controller.this.alertThreshold) {
                        this.alertSent = false;
                    }
                    Controller.this.view.updateValue(value);
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    LOGGER.error("Thread has been interrupted ", ex);
                }
            }
        }

        /**
         * External command to stop monitoring.
         */
        public void stopMonitoring() {
            this.stop = true;
        }
    }

    private void sendAlertMail(final String subject, final String body) {
        if (this.sendEmailAlert) {
            final EMailAgent agent = this.new EMailAgent(subject, body);
            new Thread(agent).start();
        }
    }

    private class SimpleMonitorViewObserver implements MonitorViewObserver {

        @Override
        public void executeCommand(final MonitorMeCommand cmd, final String... params) {
            switch (cmd) {
            case GET_VM_LIST:
                Controller.this.view.updateAvailableVMList(this.getAvailableVMs());
                break;
            case ATTACH_TO_VM:
                this.attachAgent(params[0]);
                break;
            case STOP_MONITOR:
                this.stopAgent();
                break;
            default:
                throw new UnsupportedOperationException(cmd.toString());
            }
        }

        private List<String> getAvailableVMs() {
            final List<VirtualMachineDescriptor> vmd = VirtualMachine.list();
            return vmd.stream().peek(ds -> LOGGER.debug("{}", ds.id() + ":" + ds.displayName())).map(vm -> vm.id())
                    .collect(Collectors.toList());
        }

        private void attachAgent(final String pid) {
            LOGGER.debug("pid {}", pid);
            if (Controller.this.agent != null) {
                Controller.this.agent.stopMonitoring();
            }
            Controller.this.agent = Controller.this.new Agent(pid);
            new Thread(Controller.this.agent).start();
        }

        private void stopAgent() {
            if (Controller.this.agent != null) {
                Controller.this.agent.stopMonitoring();
            } else {
                throw new IllegalStateException("Agent not configured!");
            }
        }
    }

    private class EMailAgent implements Runnable {

        private final String subject;
        private final String body;

        public EMailAgent(final String subject, final String body) {
            this.subject = subject;
            this.body = body;
        }

        public void run() {
            final Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            final Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Controller.this.gmailUsername, Controller.this.gmailPassword);
                }
            });

            final Message message = new MimeMessage(session);
            try {
                message.setFrom(new InternetAddress(Controller.this.gmailUsername));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(Controller.this.emailRecipient));
                message.setSubject(this.subject);
                message.setText(this.body);
                Transport.send(message);
            } catch (MessagingException e) {
                LOGGER.error("Error on sending alert email ", e);
                Controller.this.view.displayError("Alert email not sent: " + e.getLocalizedMessage());
            }
        }
    }
}
