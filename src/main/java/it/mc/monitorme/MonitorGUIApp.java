package it.mc.monitorme;

import it.mc.monitorme.controller.Controller;
import it.mc.monitorme.view.MonitorGUI;
import it.mc.monitorme.view.MonitorView;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a GUI application for monitoring an MBean.
 *
 */
public final class MonitorGUIApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorGUIApp.class);

    private MonitorGUIApp() {
    }

    /**
     * 
     * @param args
     *            unused
     * 
     */
    public static void main(final String... args) {
        final MonitorView view = new MonitorGUI();
        final Properties props = new Properties();
        try {
            props.load(MonitorGUIApp.class.getClassLoader().getResourceAsStream("config.properties"));
        } catch (Exception e) {
            LOGGER.error("Error on app configuration, the system will quit. ", e);
            System.exit(1);
        }
        new Controller(view, props);
    }

}
