package it.mc.monitorme;

/**
 * 
 * @author mcasadei
 *
 */
public enum MonitorMeCommand {

    /**
     * Used to retrieve a list of vms available on this platform.
     */
    GET_VM_LIST,

    /**
     * Use to attach to a specific vm and start monitoring.
     */
    ATTACH_TO_VM,

    /**
     * Emitted in order to stop monitor on a specific VM.
     */
    STOP_MONITOR;
}
