package it.mc.monitorme.view;

import it.mc.monitorme.MonitorMeCommand;

/**
 * Define the interface of an observer "listening" for something to occur on the
 * view.
 *
 */
public interface MonitorViewObserver {

    /**
     * Execute a command.
     * 
     * @param cmd
     *            the command to be executed
     * @param params
     *            a set of params associated with the comand
     * @throws OperationNotSupportedException
     *             if an unsupported command is received
     */
    void executeCommand(final MonitorMeCommand cmd, final String... params);

}
