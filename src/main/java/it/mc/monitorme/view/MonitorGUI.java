package it.mc.monitorme.view;

import it.mc.monitorme.MonitorMeCommand;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An implementation of {@link MonitorView}.
 *
 */
@SuppressWarnings("restriction")
public class MonitorGUI extends JFrame implements MonitorView {

    private static final long serialVersionUID = -6218820567019985015L;
    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorGUI.class);

    private static final double WIDTH_PERC = 0.5;
    private static final double HEIGHT_PERC = 0.5;
    private static final int KB = 1024;
    private static final int MS = 1000;

    private final JComboBox<String> vmList = new JComboBox<>();
    private final JLabel vmLabel = new JLabel("Choose a locally running JVM (PID) to monitor");
    private final JButton start = new JButton("monitor");
    private final JButton stop = new JButton("stop");
    private final JFXPanel chartFxPanel = new JFXPanel();
    private long monitorStartTime;
    private LineChart<Number, Number> lineChart;
    private XYChart.Series<Number, Number> series;

    private final Set<MonitorViewObserver> observers;

    /**
     * Builds a new CGUI.
     */
    public MonitorGUI() {
        super();
        this.observers = new HashSet<>();

        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        chartFxPanel.setSize((int) this.getSize().getWidth(), (int) this.getSize().getHeight());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        final JPanel panel = new JPanel();
        final LayoutManager layout = new BorderLayout();
        panel.setLayout(layout);
        final JPanel pNorth = new JPanel();
        pNorth.add(vmLabel);
        pNorth.add(vmList);
        pNorth.add(start);
        pNorth.add(stop);
        final JPanel pCenter = new JPanel();
        pCenter.add(chartFxPanel);
        panel.add(pNorth, BorderLayout.NORTH);
        panel.add(pCenter, BorderLayout.CENTER);

        this.getContentPane().add(panel);
        this.setTitle("Monitor ME - JVM Heap Memory Trend");

        stop.addActionListener(e -> {
            this.observers.forEach(o -> o.executeCommand(MonitorMeCommand.STOP_MONITOR));
            MonitorGUI.this.updateViewState(ViewState.STOPPED);
        });

        start.addActionListener(e -> {
            this.observers.forEach(o -> o.executeCommand(MonitorMeCommand.ATTACH_TO_VM, MonitorGUI.this.vmList
                    .getSelectedItem().toString()));
            MonitorGUI.this.updateViewState(ViewState.STARTED);
        });
    }

    /**
     * {@inheritDoc} .
     */
    public void initView() {
        for (final MonitorViewObserver observer : observers) {
            observer.executeCommand(MonitorMeCommand.GET_VM_LIST);
        }
        this.updateViewState(ViewState.INIT);
        this.setVisible(true);
    }

    /**
     * {@inheritDoc} .
     */
    public void updateAvailableVMList(final List<String> vmNames) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                vmNames.stream().forEach(vm -> MonitorGUI.this.vmList.addItem(vm));
            }
        });
    }

    /**
     * {@inheritDoc} .
     */
    public void updateValue(final long value) {
        try {
            SwingUtilities.invokeAndWait(() -> MonitorGUI.this.updateChart(value));
        } catch (InvocationTargetException | InterruptedException e) {
            LOGGER.error("Error updating view {}", e);
        }
    }

    private void updateChart(final Long value) {
        Platform.runLater(() -> {
            MonitorGUI.this.lineChart.setTitle("Heap Current Usage " + Long.toString(value / KB) + " KB");
            MonitorGUI.this.series.getData().add(
                    new XYChart.Data<Number, Number>((System.currentTimeMillis() - MonitorGUI.this.monitorStartTime)
                            / MS, value / KB));
        });
    }

    private void initMonitorChart() {
        this.monitorStartTime = System.currentTimeMillis();
        Platform.runLater(() -> MonitorGUI.this.initFX(MonitorGUI.this.chartFxPanel));
    }

    /**
     * {@inheritDoc} .
     */
    public void addMonitorViewObserver(final MonitorViewObserver view) {
        this.observers.add(view);
    }

    /**
     * {@inheritDoc} .
     */
    public void displayError(final String msg) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JOptionPane.showMessageDialog(MonitorGUI.this, msg, "Monitor error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    /**
     * {@inheritDoc} .
     */
    public void displayAlert(final String msg) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JOptionPane.showMessageDialog(MonitorGUI.this, msg, "Monitor warning", JOptionPane.WARNING_MESSAGE);
            }
        });
    }

    private void updateViewState(final ViewState state) {
        switch (state) {
        case STARTED:
            MonitorGUI.this.start.setEnabled(false);
            MonitorGUI.this.stop.setEnabled(true);
            MonitorGUI.this.vmList.setEnabled(false);
            MonitorGUI.this.initMonitorChart();
            break;
        case STOPPED:
            MonitorGUI.this.start.setEnabled(true);
            MonitorGUI.this.stop.setEnabled(false);
            MonitorGUI.this.vmList.setEnabled(true);
            break;
        case INIT:
        default:
            MonitorGUI.this.start.setEnabled(true);
            MonitorGUI.this.stop.setEnabled(false);
            MonitorGUI.this.vmList.setEnabled(true);
            MonitorGUI.this.initMonitorChart();
        }
    }

    private void initFX(final JFXPanel fxPanel) {
        // This method is invoked on the JavaFX thread
        final Scene scene = createScene();
        fxPanel.setScene(scene);
    }

    private Scene createScene() {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Elapsed seconds");
        // creating the chart
        lineChart = new LineChart<Number, Number>(xAxis, yAxis);

        lineChart.setTitle("Heap Current Usage ");
        // defining a series
        this.series = new XYChart.Series<Number, Number>();
        this.series.setName("Heap Usage (KB)");

        final Scene scene = new Scene(this.lineChart, this.getSize().getWidth() * 0.9, this.getSize().getHeight() * 0.7);
        this.lineChart.getData().add(this.series);

        return scene;
    }

    private enum ViewState {
        INIT, STARTED, STOPPED;
    }
}
