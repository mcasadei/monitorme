package it.mc.monitorme.view;

import java.util.List;

/**
 * An interface for defining a view for a monitor.
 * 
 * @author mcasadei
 *
 */
public interface MonitorView {

    /**
     * 
     */
    void initView();

    /**
     * 
     * @param value
     *            the value to be shown
     */
    void updateValue(long value);

    /**
     * 
     * @param vmNames
     *            names of the found vms
     */
    void updateAvailableVMList(List<String> vmNames);

    /**
     * 
     * @param view
     *            the observer for the view
     */
    void addMonitorViewObserver(MonitorViewObserver view);

    /**
     * 
     * @param msg the error message to display
     */
    void displayError(final String msg);

    /**
     * 
     * @param msg the alert message to display
     */
    void displayAlert(final String msg);

}
