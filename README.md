This application provides an example of monitoring MBeans via JMX. Specifically, it allows to monitor Heap current occupation on any jvm running on locally.

In case the monitored values goes above a configured threshold, an alert is displayed and, optionally an email can be sent to a recipient by using a *GMAIL* account. 


#REQUIREMENTS
*JDK 1.8* is required.

In order to work, `JAVA_HOME` must point to a valid *JDK 1.8* installation.

Application build and execution have been successfully tested on *Windows 7* and *Linux Mint 17 64bit* OS.

#CONFIGURATION
Configuration can be done via `config.properties` file in `src/main/resources`. A sample configuration file is reported below:

```
alertThreshold=80000000
emailAlert=false
gmailUsername=
gmailPassword=
emailReceiver=
```
the above sets an alert threshold of 80000000 bytes (~80 MB) and upon overcoming the threshold no email is going to be sent.

In case one wants to send an email upon threshold overcoming the configuration should looks like:
```
alertThreshold=80000000
emailAlert=true
gmailUsername=yourname@gmail.com
gmailPassword=yourpassword
emailReceiver=recipient@recipient.it
```
*NOTE* In case the Gmail account is configured for 2-step authentication, an *application password* must be generated and set as the value of *gmailPassword* (see  <https://security.google.com/settings/security/apppasswords>).

#BUILD PROCESS
In order to build the application, move to the project root and type:
```
mvn clean install
```

#RUN THE APPLICATION
In order to run the application, from the project root, type:
```
java -jar target/monitorme-0.4-jar-with-all-dependencies.jar
```

#GIT REPO
<https://bitbucket.org/mcasadei/monitorme>
 

